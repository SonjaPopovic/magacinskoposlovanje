angular.module("magacinApp").controller("addArtiklCrtr", function ($scope, $http,$location){
	$scope.osobe=[];
	var getOsobe=function(){
		$http.get("/api/osobe")
		.then(
				function success(res){
				$scope.osobe=res.data;
				},
				function error(res){
					console.log(res);
					alert("Neuspesno dobavljanje osoba");
				});
	}
	getOsobe();
	
	$scope.kategorije=[];
	var getKategorije= function(){
		$http.get("/api/kategorije")
		.then(
				function success(res){
				$scope.kategorije=res.data;
				},
				function error(res){
					console.log(res);
					alert("Neuspesno dobavljanje kategorija");
				});
	}
	getKategorije();
	
	$scope.novi={};
	
	$scope.dodajArtikl = function (idOsobe){
		
		
		$http.post("/api/artikli/"+idOsobe,$scope.novi)
		.then(
				function success(res){
					
				$location.path("/artikli")
				},
				function error(res){
					alert("Neuspesno dodavanje artikla!");
					console.log(res);
				});
	}
	
	
});