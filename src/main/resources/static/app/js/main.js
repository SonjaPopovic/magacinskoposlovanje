var app = angular.module('magacinApp',['ngRoute']);
app.controller("pocetnaStranica",function($scope){
	$scope.firma="Zmajevo d.o.o";
});
 



app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html'
		})
		.when('/artikli', {
			templateUrl : '/app/html/partial/artikli.html'
		})
		.when('/kategorije', {
			templateUrl : '/app/html/partial/kategorije.html'
		})
		.when('/osobe', {
			templateUrl: '/app/html/partial/osobe.html'
		})
		.when('/artikli/edit/:id', {
			templateUrl:'/app/html/partial/artikli-edit.html'
				})
				
		.when('/artikli/add', {
			templateUrl:'/app/html/partial/artikli-add.html'
				})
				.when('/statistika', {
			templateUrl:'/app/html/partial/statistika.html'
				})
		.otherwise({
			redirectTo: '/'
		});
}]);
