angular.module("magacinApp").controller("editArtiklCtrl", function($scope,$http,$routeParams,$location){
	
	$scope.osobe=[];
	var getOsobe=function(){
		$http.get("/api/osobe")
		.then(
				function success(res){
				$scope.osobe=res.data;
				},
				function error(res){
					console.log(res);
					alert("Neuspesno dobavljanje osoba");
				});
	}
	getOsobe();
	
	
	$scope.artikl={};
	
	var getArtikl = function (){
		$http.get("/api/artikli/"+$routeParams.id)
		.then (
				function success(res){
					$scope.artikl=res.data;
				},
				function error(res){
					alert("Ne postojeci artikl");
				});
		
	}
	getArtikl();
	
	$scope.nova={};
	$scope.kolicina=""
	$scope.promene=[{id:"1",opis:"UVECAJ STANJE"},{id:"2",opis:"UMANJI STANJE"}];
	
	$scope.izmeni=function(){
		$scope.nova.kolicina;
		$scope.nova.artiklId=$scope.artikl.id;
		$scope.nova.datumIzmene=$scope.artikl.datumPoslednjeIzmene;
		$scope.nova.stanjePreIzmene=$scope.artikl.stanje;
		
		if($scope.nova.opisIzmene=="UMANJI STANJE"){
			$scope.nova.kolicina=-$scope.nova.kolicina;
			
		}
		
		if(($scope.artikl.stanje+$scope.nova.kolicina)<0){
			alert("Nedovoljno artikala na stanju!");
			$scope.nova={};
			return;
		}
		
		$http.post("/api/izmene",$scope.nova)
		.then(
				function success(res){


					$location.path("/artikli");
					
				},
				function error(res){
					alert("Neuspesna izmena!");
				});
	}
	
});
