package jwd.magacin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.magacin.dto.ArtiklDTO;
import jwd.magacin.dto.KategorijaDTO;
import jwd.magacin.model.Artikl;
import jwd.magacin.model.Kategorija;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.KategorijaService;
import jwd.magacin.support.ArtiklToArtiklDTO;
import jwd.magacin.support.KategorijaDTOtoKategorija;
import jwd.magacin.support.KategorijaToKategorijaDTO;

@RestController
@RequestMapping(value = "/api/kategorije")
public class KategorijaController {
	@Autowired
	KategorijaService kategorijaService;
	@Autowired
	ArtiklService artiklService;
	@Autowired
	KategorijaToKategorijaDTO toDto;
	@Autowired
	KategorijaDTOtoKategorija toKategorija;
	@Autowired
	ArtiklToArtiklDTO toArtiklDTO;
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<KategorijaDTO>> get() {
		List<Kategorija> lista = kategorijaService.findAll();
		return new ResponseEntity<>(toDto.convert(lista), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<KategorijaDTO> get(@PathVariable Long id) {

		Kategorija kategorija = kategorijaService.findOne(id);

		if (kategorija == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(kategorija), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<KategorijaDTO> delete(@PathVariable Long id) {

		Kategorija k = kategorijaService.findOne(id);

		if (k == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		kategorijaService.remove(id);

		return new ResponseEntity<>(toDto.convert(k), HttpStatus.OK);
	}

	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<KategorijaDTO> addNew(@RequestBody KategorijaDTO novi) {
		kategorijaService.save(toKategorija.convert(novi));

		if (novi == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(novi, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<KategorijaDTO> edit(@RequestBody KategorijaDTO nova,
			@PathVariable Long id) {

		if (id != nova.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		kategorijaService.save(toKategorija.convert(nova));

		return new ResponseEntity<>(nova, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.GET, value="/{id}/artikli")
	public ResponseEntity<List<ArtiklDTO>> getArtikliKategorije(@PathVariable Long id){
		
		
		List<Artikl> artikliKategorije = artiklService.findByKategorijaId(id);
		
		return new ResponseEntity<>(toArtiklDTO.convert(artikliKategorije),HttpStatus.OK);
		
	}


}
