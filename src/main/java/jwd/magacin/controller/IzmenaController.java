package jwd.magacin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.magacin.dto.IzmenaDTO;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.IzmenaService;
import jwd.magacin.support.IzmenaDTOtoIzmena;
import jwd.magacin.support.IzmenaToIzmenaDTO;

@RestController
@RequestMapping(value="/api/izmene")
public class IzmenaController {
 @Autowired
 IzmenaService izmenaService;
 @Autowired
 IzmenaToIzmenaDTO toDto;
 @Autowired
 IzmenaDTOtoIzmena toIzmena;
 @Autowired
 ArtiklService artiklService;
 @RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<IzmenaDTO>> get() {
		return new ResponseEntity<>(toDto.convert(izmenaService.findAll()), HttpStatus.OK);
	}
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<IzmenaDTO> get(
			@PathVariable Long id){
		
		IzmenaDTO izmena = toDto.convert(izmenaService.findOne(id));
		
		if(izmena == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				izmena,
				HttpStatus.OK);
	}
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<IzmenaDTO> delete(
			@PathVariable Long id){
		
		IzmenaDTO izmena =toDto.convert( izmenaService.findOne(id));
		
		if(izmena == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}izmenaService.remove(id);
		
		return new ResponseEntity<>(
				izmena,
				HttpStatus.OK);
	}
	@RequestMapping( method=RequestMethod.POST)
	public ResponseEntity<IzmenaDTO> addNew(
			@RequestBody IzmenaDTO nova){
		if(nova == null){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		izmenaService.save(toIzmena.convert(nova));
		
		
		return new ResponseEntity<>(
				nova,
				HttpStatus.OK);
	}
}
