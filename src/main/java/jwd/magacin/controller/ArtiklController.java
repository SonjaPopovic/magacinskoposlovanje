package jwd.magacin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.Headers;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.magacin.dto.ArtiklDTO;
import jwd.magacin.model.Artikl;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.support.ArtiklDTOtoArtikl;
import jwd.magacin.support.ArtiklToArtiklDTO;


@RestController
@RequestMapping(value = "/api/artikli")
public class ArtiklController {
	@Autowired
	ArtiklService artiklService;
	@Autowired
	ArtiklDTOtoArtikl toArtikl;
	@Autowired
	ArtiklToArtiklDTO toDto;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ArtiklDTO>> get(@RequestParam (defaultValue="0") int page,
			@RequestParam(required=false) Long kategorijaId,
			@RequestParam(required=false) String artiklNaziv) {
		
		Page<Artikl> pageArtikl;
		if(kategorijaId != null || artiklNaziv != null){
			pageArtikl = artiklService.findAll(kategorijaId, artiklNaziv,page );
		}
		pageArtikl = artiklService.findAll(page);
		
		List<Artikl> lista = pageArtikl.getContent();
		HttpHeaders headers= new HttpHeaders();
		headers.add("totalPages", Integer.toString(pageArtikl.getTotalPages()));
		return new ResponseEntity<>(toDto.convert(lista), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ArtiklDTO> get(@PathVariable Long id) {

		Artikl artikl = artiklService.findOne(id);

		if (artikl == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(artikl), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ArtiklDTO> delete(@PathVariable Long id) {

		Artikl artikl = artiklService.findOne(id);

		if (artikl == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		artiklService.remove(id,1l);

		return new ResponseEntity<>(toDto.convert(artikl), HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.POST)
	public ResponseEntity<ArtiklDTO> addNew(@RequestBody ArtiklDTO novi, @PathVariable Long id) {
		artiklService.save(toArtikl.convert(novi),id);

		if (novi == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(novi, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<ArtiklDTO> edit(@RequestBody ArtiklDTO artikl,
			@PathVariable Long id) {

		if (id != artikl.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		artiklService.save(toArtikl.convert(artikl));

		return new ResponseEntity<>(artikl, HttpStatus.OK);
	}
}
