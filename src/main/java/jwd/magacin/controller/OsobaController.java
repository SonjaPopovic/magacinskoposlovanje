package jwd.magacin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.magacin.dto.KategorijaDTO;
import jwd.magacin.dto.OsobaDTO;
import jwd.magacin.model.Kategorija;
import jwd.magacin.service.KategorijaService;
import jwd.magacin.service.OsobaService;
import jwd.magacin.support.KategorijaToKategorijaDTO;
import jwd.magacin.support.OsobaDTOtoOsoba;
import jwd.magacin.support.OsobaToOsobaDTO;

@RestController
@RequestMapping(value = "/api/osobe")
public class OsobaController {
	@Autowired
	OsobaService osobaService;
	@Autowired
	OsobaDTOtoOsoba toOsoba;
	@Autowired
	OsobaToOsobaDTO toDto;
	@Autowired 
	KategorijaService kategorijaService;
	@Autowired
	KategorijaToKategorijaDTO toKategorijaDTO;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<OsobaDTO>> get() {
		return new ResponseEntity<>(toDto.convert(osobaService.findAll()), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<OsobaDTO> get(@PathVariable Long id) {

		OsobaDTO osoba = toDto.convert(osobaService.findOne(id));

		if (osoba == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(osoba, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<OsobaDTO> delete(@PathVariable Long id) {

		OsobaDTO osoba = toDto.convert(osobaService.findOne(id));

		if (osoba == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		osobaService.remove(id);

		return new ResponseEntity<>(osoba, HttpStatus.OK);
	}

	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<OsobaDTO> addNew(@RequestBody OsobaDTO nova) {
		osobaService.save(toOsoba.convert(nova));

		if (nova == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(nova, HttpStatus.OK);
	}
	@RequestMapping (method = RequestMethod.GET, value="/{id}/kategorije")
	public ResponseEntity<List<KategorijaDTO>> getKategorijeOsobe (@PathVariable Long id){
		
		List<Kategorija> kategorije = kategorijaService.findByNadlezneOsobeId(id);
		
		return new ResponseEntity<>(toKategorijaDTO.convert(kategorije),HttpStatus.OK);
		
		
	}

}
