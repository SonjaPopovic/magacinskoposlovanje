package jwd.magacin.dto;

import java.util.Date;

public class IzmenaDTO {
	private Long id;

	private String opisIzmene;

	private Date datumIzmene;

	private Double kolicina;

	private Long osobaId;

	private Long artiklId;
	private Double stanjePreIzmene;

public IzmenaDTO() {
	// TODO Auto-generated constructor stub
}

public IzmenaDTO( String opisIzmene, Date datumIzmene, Double kolicina, Long osobaId, Long artiklId, double stanjePreIzmene) {
	super();
	
	this.opisIzmene = opisIzmene;
	this.datumIzmene = datumIzmene;
	this.kolicina = kolicina;
	this.osobaId = osobaId;
	this.artiklId = artiklId;
	this.stanjePreIzmene = stanjePreIzmene;
}

public Double getStanjePreIzmene() {
	return stanjePreIzmene;
}

public void setStanjePreIzmene(Double stanjePreIzmene) {
	this.stanjePreIzmene = stanjePreIzmene;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getOpisIzmene() {
	return opisIzmene;
}

public void setOpisIzmene(String opisIzmene) {
	this.opisIzmene = opisIzmene;
}

public Date getDatumIzmene() {
	return datumIzmene;
}

public void setDatumIzmene(Date datumIzmene) {
	this.datumIzmene = datumIzmene;
}

public Double getKolicina() {
	return kolicina;
}

public void setKolicina(Double kolicina) {
	this.kolicina = kolicina;
}

public Long getOsobaId() {
	return osobaId;
}

public void setOsobaId(Long osobaId) {
	this.osobaId = osobaId;
}

public Long getArtiklId() {
	return artiklId;
}

public void setArtiklId(Long artiklId) {
	this.artiklId = artiklId;
}
}
