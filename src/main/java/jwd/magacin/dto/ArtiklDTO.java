package jwd.magacin.dto;

import java.util.Date;

public class ArtiklDTO {
	private Long id;

	private String opis;

	private Double stanje;

	private Date datumPoslednjeIzmene;
	private double minimalnaKolicina;
	private boolean obrisan;
	private Long kategorijaId;
	private String kategorijaNaziv;
	private String jedinicaMere;

	public String getKategorijaNaziv() {
		return kategorijaNaziv;
	}

	public void setKategorijaNaziv(String kategorijaNaziv) {
		this.kategorijaNaziv = kategorijaNaziv;
	}

	public Long getKategorijaId() {
		return kategorijaId;
	}

	public void setKategorijaId(Long kategorijaId) {
		this.kategorijaId = kategorijaId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Date getDatumPoslednjeIzmene() {
		return datumPoslednjeIzmene;
	}

	public void setDatumPoslednjeIzmene(Date datumPoslednjeIzmene) {
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
	}

	public double getMinimalnaKolicina() {
		return minimalnaKolicina;
	}

	public void setMinimalnaKolicina(double minimalnaKolicina) {
		this.minimalnaKolicina = minimalnaKolicina;
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}

	public ArtiklDTO(Long id, String opis, Double stanje, Date datumPoslednjeIzmene, boolean obrisan,
			double minimalnaKolicina, Long kategorijaId) {
		super();
		this.id = id;
		this.opis = opis;
		this.stanje = stanje;
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
		this.obrisan = obrisan;
		this.minimalnaKolicina = minimalnaKolicina;
		this.kategorijaId = kategorijaId;
	}

	public ArtiklDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}

	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}

}
