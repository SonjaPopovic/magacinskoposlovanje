package jwd.magacin.dto;



public class OsobaDTO {
	
	private Long id;

	private String ime;
	
	private String prezime;
	
	private String radnoMesto;	

	private String userName;

	private String password;
	

public OsobaDTO() {
	// TODO Auto-generated constructor stub
}
	public OsobaDTO(Long id, String ime, String prezime, String radnoMesto, String userName, String password) {
	super();
	this.id = id;
	this.ime = ime;
	this.prezime = prezime;
	this.radnoMesto = radnoMesto;
	this.userName = userName;
	this.password = password;
	
}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getRadnoMesto() {
		return radnoMesto;
	}

	public void setRadnoMesto(String radnoMesto) {
		this.radnoMesto = radnoMesto;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	
	
	
}
