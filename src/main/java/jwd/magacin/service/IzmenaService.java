package jwd.magacin.service;

import java.util.List;

import jwd.magacin.model.Artikl;
import jwd.magacin.model.Izmena;

public interface IzmenaService {
	Izmena findOne(Long id);
	List<Izmena> findAll();
	void save(Izmena izmena);
	void remove(Long id);
}
