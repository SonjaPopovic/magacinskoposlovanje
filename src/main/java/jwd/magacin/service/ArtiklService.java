package jwd.magacin.service;

import java.util.List;

import org.springframework.data.domain.Page;

import jwd.magacin.model.Artikl;


public interface ArtiklService {
	Artikl findOne(Long id);
	List<Artikl> findAll();
	void save(Artikl artikl, Long osobaId);
	void remove(Long id, Long osobaId);
	void save(Artikl artikl);
	List<Artikl> findByKategorijaId(Long id);
	Page<Artikl> findAll(int page);
	Page<Artikl> findAll(Long kategorijaId, String artiklNaziv, int page);
}
