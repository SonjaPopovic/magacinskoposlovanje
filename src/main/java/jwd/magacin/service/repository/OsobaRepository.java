package jwd.magacin.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.magacin.model.Osoba;
@Repository
public interface OsobaRepository extends JpaRepository<Osoba, Long> {

}
