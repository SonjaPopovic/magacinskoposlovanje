package jwd.magacin.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.magacin.model.Kategorija;
@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija, Long> {

	List<Kategorija> findByNadlezneOsobeId(Long osobaId);

}
