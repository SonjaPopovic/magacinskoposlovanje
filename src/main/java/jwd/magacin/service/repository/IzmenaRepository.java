package jwd.magacin.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.magacin.model.Izmena;
@Repository
public interface IzmenaRepository extends JpaRepository<Izmena, Long> {

}
