package jwd.magacin.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import jwd.magacin.model.Artikl;
@Repository
public interface ArtiklRepository extends JpaRepository<Artikl, Long> {

	List<Artikl> findByKategorijaId(Long id);

//	List<Artikl> findByKategorijaId(Long id);
//@Query("SELECT a FROM Artikl a WHERE"
//		+""
//		)
//	//Page<Artikl> findAll(@Param ()Long kategorijaId, String artiklNaziv, PageRequest pageRequest);

}
