 package jwd.magacin.service.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.magacin.model.Artikl;
import jwd.magacin.model.Izmena;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.repository.ArtiklRepository;
import jwd.magacin.service.repository.IzmenaRepository;
import jwd.magacin.service.repository.OsobaRepository;

@Service
public class JpaArtiklServiceImpl implements ArtiklService {
	@Autowired
	ArtiklRepository artiklReposiory;
	@Autowired
	IzmenaRepository izmenaRepository;
	@Autowired
	OsobaRepository osobaRepository;

	@Override
	public Artikl findOne(Long id) {
		// TODO Auto-generated method stub
		return artiklReposiory.findOne(id);
	}

	@Override
	public List<Artikl> findAll() {
		// TODO Auto-generated method stub
		List<Artikl> artikli = artiklReposiory.findAll();
		List<Artikl> filter = new ArrayList<Artikl>();
		for (Artikl a : artikli) {
			if (!a.isObrisan()) {
				filter.add(a);
			}
		}
		return filter;
	}

	@Override
	public void save(Artikl artikl, Long osobaId) {
		// TODO Auto-generated method stub
		try {
			artiklReposiory.save(artikl);

			Izmena i = new Izmena("Inicijalno dodavanje artikla", artikl.getDatumPoslednjeIzmene(), artikl.getStanje(),
					0.0, osobaRepository.findOne(osobaId), artikl);
			izmenaRepository.save(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void remove(Long id, Long osobaId) {
		// TODO Auto-generated method stub
		Artikl a = artiklReposiory.findOne(id);
		Izmena i = new Izmena("Krajnje brisanje artikla.",new Date(),-a.getStanje(),a.getStanje(),osobaRepository.findOne(osobaId),a);
		
		try {
			a.setObrisan(true);
			izmenaRepository.save(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void save(Artikl artikl) {
		// TODO Auto-generated method stub
		artiklReposiory.save(artikl);
	}

	@Override
	public List<Artikl> findByKategorijaId(Long id) {
		// TODO Auto-generated method stub
		return artiklReposiory.findByKategorijaId(id);
	}

	@Override
	public Page<Artikl> findAll(int page) {
		// TODO Auto-generated method stub
		return artiklReposiory.findAll(new PageRequest(page, 2));
	}

	@Override
	public Page<Artikl> findAll(Long kategorijaId, String artiklNaziv, int page) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Page<Artikl> findAll(Long kategorijaId, String artiklNaziv, int page) {
//		// TODO Auto-generated method stub
//	if(artiklNaziv!=null){artiklNaziv="%" + artiklNaziv + "%";}
//		return artiklReposiory.findAll(kategorijaId,artiklNaziv,new PageRequest(page, 2));
//	}

}
