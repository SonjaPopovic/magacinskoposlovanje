package jwd.magacin.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.magacin.model.Osoba;
import jwd.magacin.service.OsobaService;
import jwd.magacin.service.repository.OsobaRepository;

@Service
public class JpaOsobaServiceImpl implements OsobaService {
	@Autowired
	OsobaRepository osobaRepository;

	@Override
	public Osoba findOne(Long id) {
		// TODO Auto-generated method stub
		return osobaRepository.findOne(id);
	}

	@Override
	public List<Osoba> findAll() {
		// TODO Auto-generated method stub
		return osobaRepository.findAll();
	}

	@Override
	public void save(Osoba osoba) {
		// TODO Auto-generated method stub
		osobaRepository.save(osoba);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		osobaRepository.delete(id);
	}

}
