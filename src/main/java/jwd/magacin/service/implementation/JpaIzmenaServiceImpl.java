package jwd.magacin.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.magacin.model.Artikl;
import jwd.magacin.model.Izmena;
import jwd.magacin.service.IzmenaService;
import jwd.magacin.service.repository.ArtiklRepository;
import jwd.magacin.service.repository.IzmenaRepository;

@Service
public class JpaIzmenaServiceImpl implements IzmenaService {
	@Autowired
	IzmenaRepository izmenaRepository;
	@Autowired
	ArtiklRepository artiklRepository;

	@Override
	public Izmena findOne(Long id) {
		// TODO Auto-generated method stub
		return izmenaRepository.findOne(id);
	}

	@Override
	public List<Izmena> findAll() {
		// TODO Auto-generated method stub
		return izmenaRepository.findAll();
	}

	//prilikom izmene artikla, moraju se sacuvati i podaci o izmeni, kako bi se posle mogle pratiti promene stanja artikla
	// po datumima ili nekim drugim atributima
	@Override
	public void save(Izmena izmena) {
		// TODO Auto-generated method stub
		
		
		try {
			Artikl a = izmena.getArtikl();
			izmena.setStanjePreIzmene(a.getStanje());
			a.setStanje(a.getStanje() + izmena.getKolicina());
			a.setDatumPoslednjeIzmene(izmena.getDatumIzmene());
			
			if(a.getStanje()<0){
				return;
			}
			izmenaRepository.save(izmena);
			artiklRepository.save(a);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		izmenaRepository.delete(id);
	}

}
