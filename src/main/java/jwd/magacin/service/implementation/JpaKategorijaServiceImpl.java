package jwd.magacin.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.magacin.model.Kategorija;
import jwd.magacin.service.KategorijaService;
import jwd.magacin.service.repository.KategorijaRepository;
@Service
public class JpaKategorijaServiceImpl implements KategorijaService {
@Autowired
KategorijaRepository kategorijaRepository;
	@Override
	public Kategorija findOne(Long id) {
		// TODO Auto-generated method stub
		return kategorijaRepository.findOne(id);
	}

	@Override
	public List<Kategorija> findAll() {
		// TODO Auto-generated method stub
		return kategorijaRepository.findAll();
	}

	@Override
	public void save(Kategorija kategorija) {
		// TODO Auto-generated method stub
		kategorijaRepository.save(kategorija);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		kategorijaRepository.delete(id);
	}

	@Override
	public List<Kategorija> findByNadlezneOsobeId(Long osobaId) {
		// TODO Auto-generated method stub
		return kategorijaRepository.findByNadlezneOsobeId(osobaId);
		
	}

}
