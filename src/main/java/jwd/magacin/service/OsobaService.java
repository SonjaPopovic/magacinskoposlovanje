package jwd.magacin.service;

import java.util.List;

import jwd.magacin.model.Osoba;

public interface OsobaService {

	
	Osoba findOne(Long id);
	List<Osoba> findAll();
	void save(Osoba osoba);
	void remove(Long id);
}
