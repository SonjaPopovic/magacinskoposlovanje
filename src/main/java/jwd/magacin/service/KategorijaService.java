package jwd.magacin.service;

import java.util.List;

import jwd.magacin.model.Kategorija;

public interface KategorijaService {
	Kategorija findOne(Long id);
	List<Kategorija> findAll();
	void save(Kategorija kategorija);
	void remove(Long id);
	List<Kategorija> findByNadlezneOsobeId(Long osobaId);
}
