package jwd.magacin;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.magacin.model.Artikl;
import jwd.magacin.model.Izmena;
import jwd.magacin.model.Kategorija;
import jwd.magacin.model.Osoba;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.IzmenaService;
import jwd.magacin.service.KategorijaService;
import jwd.magacin.service.OsobaService;

@Component
public class TestData {
	@Autowired
	ArtiklService artiklService;
	@Autowired
	OsobaService osobaService;
	@Autowired
	IzmenaService izmenaService;
	@Autowired
	KategorijaService kategorijaService;

	@PostConstruct
	public void init() {
		Kategorija k = new Kategorija(1l, "proizvodnja");
		Kategorija k2 = new Kategorija(2l, "zastita na radu");
		kategorijaService.save(k);
		kategorijaService.save(k2);

		Artikl a = new Artikl(1l, "Cekic", "komada", 1.0, new Date(), false, 2.0);
		a.setKategorija(k);
		Artikl b = new Artikl(2l, "Ekser", "komada", 150.0, new Date(), false, 100.0);
		b.setKategorija(k);
		Artikl c = new Artikl(3l, "Rukavice", "komada", 3.0, new Date(), false, 2.0);
		c.setKategorija(k2);
		artiklService.save(a);
		artiklService.save(b);
		artiklService.save(c);

		Osoba o = new Osoba(1l, "Petar", "Popovic", "BZR I ZOP", "petarP", "12345");
		Osoba o1 = new Osoba(2l, "Dragan", "Vasic", "magacioner", "draganV", "1234");
		Osoba o2 = new Osoba(3l, "Mika", "Mikic", "majstor", "mikaM", "123");
		o.addKategorija(k);
		o1.addKategorija(k2);
		o1.addKategorija(k);
		o2.addKategorija(k2);
		osobaService.save(o);
		osobaService.save(o1);
		osobaService.save(o2);
		kategorijaService.save(k);
		kategorijaService.save(k2);

		Izmena i = new Izmena("prodato",new Date(),30.0,3.0,o,c);
		Izmena i2 = new Izmena("kupljeno",new Date(),150.0,150.0,o,b);
		izmenaService.save(i);
		izmenaService.save(i2);
	}

}
