package jwd.magacin.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Kategorija {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false)
	private String naziv;
	@ManyToMany
    @JoinTable(
        name = "OsobeIKategorije",
        joinColumns = {
            @JoinColumn(name = "idKategorije", nullable = false)
        },
        inverseJoinColumns = {
            @JoinColumn(name = "idOsobe", nullable = false)
        }
    )
	private List<Osoba> nadlezneOsobe = new ArrayList<>();
	@OneToMany(mappedBy="kategorija",fetch=FetchType.LAZY)
	private List<Artikl> artikliKategorije = new ArrayList<>();
	
	public Kategorija() {
		// TODO Auto-generated constructor stub
	}

	public Kategorija(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Osoba> getNadlezneOsobe() {
		return nadlezneOsobe;
	}

	public void setNadlezneOsobe(List<Osoba> nadlezneOsobe) {
		this.nadlezneOsobe = nadlezneOsobe;
	}

	public List<Artikl> getArtikliKategorije() {
		return artikliKategorije;
	}

	public void setArtikliKategorije(List<Artikl> artikliKategorije) {
		this.artikliKategorije = artikliKategorije;
	}
	public void addArtikl(Artikl artikl) {
		this.artikliKategorije.add(artikl);
		if(artikl!=null && !artikl.getKategorija().equals(this)){
			artikl.setKategorija(this);
		}
	}
	public void addOsoba(Osoba osoba) {
		this.nadlezneOsobe.add(osoba);
		if(osoba!=null && !osoba.getKategorije().contains(this)){
			osoba.getKategorije().add(this);
		}
	}

	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kategorija other = (Kategorija) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	} 
	
	
	
}
