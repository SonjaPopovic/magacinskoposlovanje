package jwd.magacin.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table
public class Osoba {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false)
	private String ime;
	@Column(nullable=false)
	private String prezime;
	@Column(nullable=false)
	private String radnoMesto;	
	@Column
	private String userName;
	@Column
	private String password;
	
	@OneToMany(mappedBy="osoba",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Izmena> izmene = new ArrayList<>();
	@ManyToMany(mappedBy="nadlezneOsobe",fetch=FetchType.EAGER)
	private List<Kategorija> kategorije = new ArrayList<>();
	public Osoba() {
		// TODO Auto-generated constructor stub
	}

	public Osoba(String ime, String prezime, String radnoMesto, String userName, String password) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.radnoMesto = radnoMesto;
		this.userName = userName;
		this.password = password;
	
	}

	public Osoba(String ime, String prezime, String radnoMesto, String userName, String password, 
			List<Izmena> izmene) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.radnoMesto = radnoMesto;
		this.userName = userName;
		this.password = password;
	
		this.izmene = izmene;
	}

	

	public Osoba(Long id, String ime, String prezime, String radnoMesto, String userName, String password) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.radnoMesto = radnoMesto;
		this.userName = userName;
		this.password = password;
	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getRadnoMesto() {
		return radnoMesto;
	}

	public void setRadnoMesto(String radnoMesto) {
		this.radnoMesto = radnoMesto;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public List<Izmena> getIzmene() {
		return izmene;
	}

	public void setIzmene(List<Izmena> izmene) {
		this.izmene = izmene;
	}
	public void addIzmena(Izmena izmena){
		this.izmene.add(izmena);
		if(!this.equals(izmena.getOsoba())){
			izmena.setOsoba(this);
		}
	}

	public List<Kategorija> getKategorije() {
		return kategorije;
	}

	public void setKategorije(List<Kategorija> kategorije) {
		this.kategorije = kategorije;
	}
	public void addKategorija(Kategorija kategorija) {
		this.kategorije.add(kategorija);
		if(kategorija!=null && !kategorija.getNadlezneOsobe().contains(this)){
			kategorija.getNadlezneOsobe().add(this);
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Osoba other = (Osoba) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
