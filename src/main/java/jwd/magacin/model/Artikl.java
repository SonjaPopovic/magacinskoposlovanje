package jwd.magacin.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Artikl {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable = false)
	private String opis;
	@Column(nullable = false)
	private String jedinicaMere;
	@Column(nullable = false)
	private Double stanje;
	@Column(nullable = false)
	// @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy/MM/dd HH:mm",
	// timezone="GMT")
	private Date datumPoslednjeIzmene;
	@Column(nullable = false)
	private boolean obrisan;
	@ManyToOne(fetch = FetchType.EAGER)
	private Kategorija kategorija;
	@Column(nullable = false)
	private double minimalnaKolicina;
	@OneToMany(mappedBy = "artikl", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Izmena> izmene = new ArrayList<>();

	public Artikl() {
		// TODO Auto-generated constructor stub
	}

	

	public Artikl(Long id, String opis, String jedinicaMere, Double stanje, Date datumPoslednjeIzmene, boolean obrisan,
			 double minimalnaKolicina) {
		super();
		this.id = id;
		this.opis = opis;
		this.jedinicaMere = jedinicaMere;
		this.stanje = stanje;
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
		this.obrisan = obrisan;
		
		this.minimalnaKolicina = minimalnaKolicina;
	}



	public Artikl(String opis, Double stanje, Date datumPoslednjeIzmene, List<Izmena> izmene) {
		super();
		this.opis = opis;
		this.stanje = stanje;
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
		this.izmene = izmene;
	}

	public Artikl(Long id, String opis, Double stanje, Date datumPoslednjeIzmene) {
		super();
		this.id = id;
		this.opis = opis;
		this.stanje = stanje;
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
	}

	public Artikl(Long id, String opis, Double stanje, Date datumPoslednjeIzmene, boolean obrisan,
			double minimalnaKolicina) {
		super();
		this.id = id;
		this.opis = opis;
		this.stanje = stanje;
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
		this.obrisan = obrisan;
		this.minimalnaKolicina = minimalnaKolicina;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Date getDatumPoslednjeIzmene() {
		return datumPoslednjeIzmene;
	}

	public void setDatumPoslednjeIzmene(Date datumPoslednjeIzmene) {
		this.datumPoslednjeIzmene = datumPoslednjeIzmene;
	}

	public List<Izmena> getIzmene() {
		return izmene;
	}

	public void setIzmene(List<Izmena> izmene) {
		this.izmene = izmene;
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}

	public  Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
		if(kategorija!=null && !kategorija.getArtikliKategorije().contains(this)){
			kategorija.getArtikliKategorije().add(this);
		}
	}

	public double getMinimalnaKolicina() {
		return minimalnaKolicina;
	}

	public void setMinimalnaKolicina(double minimalnaKolicina) {
		this.minimalnaKolicina = minimalnaKolicina;
	}

	public String getJedinicaMere() {
		return jedinicaMere;
	}



	public void setJedinicaMere(String jedinicaMere) {
		this.jedinicaMere = jedinicaMere;
	}



	public void addIzmena(Izmena izmena) {
		this.izmene.add(izmena);
		if (!this.equals(izmena.getArtikl())) {
			izmena.setArtikl(this);
		}
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artikl other = (Artikl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
