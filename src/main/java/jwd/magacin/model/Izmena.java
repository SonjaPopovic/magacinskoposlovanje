package jwd.magacin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table
public class Izmena {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false)
	private String opisIzmene;
	@Column(nullable=false)
	private Date datumIzmene;
	@Column(nullable=false)
	private Double kolicina;
	@Column(nullable=false)
	private Double stanjePreIzmene;
	@ManyToOne(fetch=FetchType.EAGER)
	private Osoba osoba;
	@ManyToOne(fetch=FetchType.EAGER)
	private Artikl artikl;
	
	public Izmena() {
		// TODO Auto-generated constructor stub
	}

	
	public Izmena(String opisIzmene, Date datumIzmene, Double kolicina, Double stanjePreIzmene, Osoba osoba,
			Artikl artikl) {
		super();
		this.opisIzmene = opisIzmene;
		this.datumIzmene = datumIzmene;
		this.kolicina = kolicina;
		this.stanjePreIzmene = stanjePreIzmene;
		this.osoba = osoba;
		this.artikl = artikl;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpisIzmene() {
		return opisIzmene;
	}

	public void setOpisIzmene(String opisIzmene) {
		this.opisIzmene = opisIzmene;
	}

	public Date getDatumIzmene() {
		return datumIzmene;
	}

	public void setDatumIzmene(Date datumIzmene) {
		this.datumIzmene = datumIzmene;
	}

	public Double getKolicina() {
		return kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public Osoba getOsoba() {
		return osoba;
	}

	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
		if(osoba!=null && !osoba.getIzmene().contains(this)){
			osoba.getIzmene().add(this);
		}
	}

	public Artikl getArtikl() {
		return artikl;
	}

	public void setArtikl(Artikl artikl) {
		this.artikl = artikl;
		if(artikl!=null && !artikl.getIzmene().contains(this)){
			artikl.getIzmene().add(this);
		}
	}

	public Double getStanjePreIzmene() {
		return stanjePreIzmene;
	}

	public void setStanjePreIzmene(Double stanjePreIzmene) {
		this.stanjePreIzmene = stanjePreIzmene;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Izmena other = (Izmena) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
