package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.IzmenaDTO;
import jwd.magacin.model.Izmena;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.IzmenaService;
import jwd.magacin.service.OsobaService;
@Component
public class IzmenaDTOtoIzmena implements Converter<IzmenaDTO, Izmena> {
@Autowired
ArtiklService artiklService;
@Autowired
IzmenaService izmenaService;
@Autowired
OsobaService osobaService;

	@Override
	public Izmena convert(IzmenaDTO idto) {
		// TODO Auto-generated method stub
		
		Izmena retVal;
		
		if(idto.getId()==null){
			retVal = new Izmena();
			retVal.setArtikl(artiklService.findOne(idto.getArtiklId()));
			retVal.setOsoba(osobaService.findOne(idto.getOsobaId()));
			retVal.setDatumIzmene(idto.getDatumIzmene());
			retVal.setStanjePreIzmene(artiklService.findOne(idto.getArtiklId()).getStanje());
			
		}else{
			retVal = izmenaService.findOne(idto.getId());
		}
		retVal.setKolicina(idto.getKolicina());
		retVal.setOpisIzmene(idto.getOpisIzmene());
		
		
		return retVal;
	}
	
	public List<Izmena> covertList (List<IzmenaDTO> dtos){
		
		List<Izmena> retVal = new ArrayList<>();
		for(IzmenaDTO i:dtos){
			retVal.add(convert(i));
			
		}
		return retVal;
		
	}

}
