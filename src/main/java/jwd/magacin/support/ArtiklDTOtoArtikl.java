package jwd.magacin.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.ArtiklDTO;
import jwd.magacin.model.Artikl;
import jwd.magacin.service.ArtiklService;
import jwd.magacin.service.KategorijaService;
@Component
public class ArtiklDTOtoArtikl implements Converter<ArtiklDTO, Artikl> {
@Autowired
ArtiklService artiklService;
@Autowired
KategorijaService kategorijaService;
	@Override
	public Artikl convert(ArtiklDTO adto) {
		// TODO Auto-generated method stub
		Artikl retVal;
		if(adto.getId()!=null){
			retVal=artiklService.findOne(adto.getId());
			
		}else{
			retVal = new Artikl();
			
		}
		retVal.setJedinicaMere(adto.getJedinicaMere());
		retVal.setOpis(adto.getOpis());
		retVal.setDatumPoslednjeIzmene(adto.getDatumPoslednjeIzmene());
		retVal.setStanje(adto.getStanje());
		retVal.setMinimalnaKolicina(adto.getMinimalnaKolicina());
		retVal.setObrisan(adto.isObrisan());
		retVal.setKategorija(kategorijaService.findOne(adto.getKategorijaId()));
		return retVal;
	}

}
