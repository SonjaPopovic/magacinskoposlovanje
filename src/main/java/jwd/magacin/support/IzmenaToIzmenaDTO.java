package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.IzmenaDTO;
import jwd.magacin.model.Izmena;
@Component
public class IzmenaToIzmenaDTO implements Converter<Izmena, IzmenaDTO> {

	@Override
	public IzmenaDTO convert(Izmena i) {
		// TODO Auto-generated method stub

		IzmenaDTO retVal = new IzmenaDTO();
		retVal.setArtiklId(i.getArtikl().getId());
		retVal.setDatumIzmene(i.getDatumIzmene());
		retVal.setKolicina(i.getKolicina());
		retVal.setOpisIzmene(i.getOpisIzmene());
		retVal.setOsobaId(i.getOsoba().getId());
		retVal.setStanjePreIzmene(i.getStanjePreIzmene());
		return retVal;
	}

	public List<IzmenaDTO> convert(List<Izmena> is) {
		// TODO Auto-generated method stub
		List<IzmenaDTO> retVal = new ArrayList<>();
		for (Izmena i : is) {
			retVal.add(convert(i));
		}
		return retVal;
	}

}
