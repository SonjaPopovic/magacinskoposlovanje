package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.KategorijaDTO;
import jwd.magacin.model.Kategorija;
import jwd.magacin.service.KategorijaService;
@Component
public class KategorijaDTOtoKategorija implements Converter<KategorijaDTO, Kategorija> {
@Autowired
KategorijaService kategorijaService;
	@Override
	public Kategorija convert(KategorijaDTO arg0) {
		// TODO Auto-generated method stub
		Kategorija retVal;
		if(arg0.getId()!=null){
			retVal=kategorijaService.findOne(arg0.getId());
		}else{
			retVal= new Kategorija();
		}
		retVal.setNaziv(arg0.getNaziv());
		return retVal;
	}
	public List<Kategorija> convert(List<KategorijaDTO> arg) {
		// TODO Auto-generated method stub
		List<Kategorija> retVal= new ArrayList<>();
		for(KategorijaDTO kdto:arg){
			retVal.add(convert(kdto));
		}
		return retVal;
	}

}
