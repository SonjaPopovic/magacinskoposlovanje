package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.KategorijaDTO;
import jwd.magacin.model.Kategorija;
@Component
public class KategorijaToKategorijaDTO implements Converter<Kategorija, KategorijaDTO> {

	@Override
	public KategorijaDTO convert(Kategorija arg0) {
		// TODO Auto-generated method stub
		KategorijaDTO retVal = new KategorijaDTO(arg0.getId(), arg0.getNaziv());
		return retVal;
	}
	public List<KategorijaDTO> convert(List<Kategorija> arg) {
		// TODO Auto-generated method stub
		List<KategorijaDTO> retVal = new ArrayList<>();
		for(Kategorija k:arg){
			retVal.add(convert(k));
		}
		return retVal;
	}

}
