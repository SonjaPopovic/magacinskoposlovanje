package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.OsobaDTO;
import jwd.magacin.model.Osoba;
@Component
public class OsobaToOsobaDTO implements Converter<Osoba, OsobaDTO> {

	@Override
	public OsobaDTO convert(Osoba o) {
		// TODO Auto-generated method stub
		OsobaDTO retVal = new OsobaDTO(o.getId(),o.getIme(),o.getPrezime(),o.getRadnoMesto(),o.getUserName(),o.getPassword());

		return retVal;
	}

	public List<OsobaDTO> convert(List<Osoba> os) {
		// TODO Auto-generated method stub
		List<OsobaDTO> retVal = new ArrayList<>();
		 for(Osoba o:os){
			 retVal.add(convert(o));
		 }

		return retVal;
	}

}
