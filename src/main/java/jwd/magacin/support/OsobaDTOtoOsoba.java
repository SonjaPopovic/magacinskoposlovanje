package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.OsobaDTO;
import jwd.magacin.model.Osoba;
import jwd.magacin.service.OsobaService;
@Component
public class OsobaDTOtoOsoba implements Converter<OsobaDTO, Osoba> {
@Autowired
OsobaService osobaService;
	@Override
	public Osoba convert(OsobaDTO o) {
		// TODO Auto-generated method stub
		
		Osoba retVal;
		
		if(o.getId()==null){
			retVal = new Osoba();
			retVal.setIme(o.getIme());
			retVal.setPrezime(o.getPrezime());
			retVal.setUserName(o.getUserName());
			
		}else{
			retVal = osobaService.findOne(o.getId());
		}
		retVal.setPassword(o.getPassword());
	
		retVal.setRadnoMesto(o.getRadnoMesto());
		
		return retVal;
	}
	
	public List<Osoba> convert(List<OsobaDTO> os){
		List<Osoba> retVal = new ArrayList<>();
		for(OsobaDTO odto:os){
			retVal.add(convert(odto));
		}
		return retVal;
	}

}
