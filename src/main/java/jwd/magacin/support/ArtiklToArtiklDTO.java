package jwd.magacin.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.magacin.dto.ArtiklDTO;
import jwd.magacin.model.Artikl;
@Component
public class ArtiklToArtiklDTO implements Converter<Artikl, ArtiklDTO> {

	@Override
	public ArtiklDTO convert(Artikl a) {
		// TODO Auto-generated method stub
		
		ArtiklDTO retVal = new ArtiklDTO();
		retVal = new ArtiklDTO(a.getId(),a.getOpis(),a.getStanje(),a.getDatumPoslednjeIzmene(),a.isObrisan(),a.getMinimalnaKolicina(),a.getKategorija().getId());
		retVal.setKategorijaNaziv(a.getKategorija().getNaziv());
		retVal.setJedinicaMere(a.getJedinicaMere());
		return retVal;
	}
	public List<ArtiklDTO> convert(List<Artikl> a) {
		// TODO Auto-generated method stub
		
		List<ArtiklDTO> retVal = new ArrayList<>();
		for(Artikl artikl:a){
			
			retVal.add(convert(artikl));
		}
		
		return retVal;
	}
}
